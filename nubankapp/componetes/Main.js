
import React from 'react';
import {StyleSheet,View,Text,Animated} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';




export default function Main(){
  /*const translateY = new Animated.Value(0);
  const onGestureEvent = Animated.event([
    {
      nativeEvent:{
        translationY: translateY,
      }
    },
    
  ],{useNativeDriver:true})
  
  function onHandlerStateChange(event){

  }*/

  return (
    <>
        <View style={styles.content}>
          
            <Animated.View style={styles.card
             
              }>
                <View style={styles.cardHeader}>
                   <Icon name="attach-money" size={24} color="#666"/>
                   <Icon name="visibility-off" size={24} color="#666"/> 
                </View>
                <View style={styles.cardMain}>
                    <Text style={styles.Txt} >Saldo Disponivel</Text>
                    <Text style={styles.TxtD}>R$ 197.611,65</Text>
                </View>
                <View style={styles.cardFooter}>
                    <Text style={styles.cardNot}>Sua Tranferencia Foi de R$ 20,00 Wellington !</Text>
                </View>
            </Animated.View>
            
          
       </View>
      
    </>
  );
};

const styles = StyleSheet.create({
  card:{
    flex:1,
    backgroundColor:'#FFF',
    borderRadius:4,
    marginHorizontal:20,
    height:100,
    marginTop:-170,
    marginBottom:-150
    
  },
  content:{
    flex:1,
    maxHeight:400,
    zIndex:5,
  },
  cardHeader:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems:'center',
    padding:10
  },
  cardFooter:{
    padding:30,
    borderRadius:4,
    backgroundColor:'#eee'
  },
  cardMain:{
    flex:1,
    paddingHorizontal:30,
    justifyContent:'center'
  },
  Txt:{
    fontSize:13,
    color:'#999'
  },
  TxtD:{
    fontSize:30,
    marginTop:3,
    color:'#333'
  },
  cardNot:{
    fontSize:12,
    color:'#333'
  }
});