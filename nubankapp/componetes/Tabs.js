import React from 'react';
import {StyleSheet,View,Text,ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function Tabs(){
  return (
    <>
      <View style={styles.container}>
        <ScrollView contentContainerStyle={styles.TabsContainer} horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={styles.tabsItem}>
            <Icon name="person-add" size={24} color="#FFF"/>
            <Text style={styles.tabText}>Indicar amigos</Text>
        </View>
        <View style={styles.tabsItem}>
          <Icon name="chat-bubble-outline" size={24} color="#FFF" />
          <Text style={styles.tabText}>Cobrar</Text>
        </View>
        <View style={styles.tabsItem}>
          <Icon name="arrow-downward" size={24} color="#FFF" />
          <Text style={styles.tabText}>Depositar</Text>
        </View>
        <View style={styles.tabsItem}>
          <Icon name="arrow-upward" size={24} color="#FFF" />
          <Text style={styles.tabText}>Transferir</Text>
        </View>
        <View style={styles.tabsItem}>
          <Icon name="lock" size={24} color="#FFF" />
          <Text style={styles.tabText}>Bloquear cartão</Text>
        </View>
       </ScrollView>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  
    container:{
      height:100,
      marginTop:200
    },
    TabsContainer:{
    paddingHorizontal:20,
    
  },
    tabsItem:{
      width:100,
      height:100,
      backgroundColor:'#7400b8',
      borderRadius:3,
      marginLeft:10,
      padding:10,
      justifyContent:'space-between', 
    },
    tabText:{
        fontSize:13,
        color:'#FFF',
    },
});