import React from 'react';
import {StyleSheet,View,Image,Text,ScrollView} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
export default function Menu(){
  return (
    <>
      <ScrollView style={styles.container}>
        <Image
        style={styles.icon}
        source={require('../Imagem/qrcode.jpg')}/>
        <View style={styles.nav}>
          <View style={styles.navItem}>
           <Icon name="help-outline" size={20} color="#FFF"/>
           <Text style={styles.Txt}> Me ajuda</Text>
          </View>
        </View>
        <View style={styles.nav}>
          <View style={styles.navItem}>
           <Icon name="person-outline" size={20} color="#FFF"/>
           <Text style={styles.Txt}> Perfil </Text>
          </View>
        </View>
        <View style={styles.nav}>
          <View style={styles.navItem}>
          <Icon name="credit-card" size={20} color="#FFF"/>
          <Text style={styles.Txt}> Configuraçao Cartao</Text>
          </View>
        </View>
        <View style={styles.nav}>
          <View style={styles.navItem}>
          <Icon name="smartphone" size={20} color="#FFF"/>
          <Text style={styles.Txt}> Configuraçao do app</Text>
          </View>
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  icon:{
    height:100,
    width:100,
    alignItems:'center',
    marginHorizontal:100
  },
  container:{
    marginHorizontal:30
  },
  nav:{
    marginVertical:10,
    borderTopWidth:0.75,
    borderTopColor:'#e8e8e4',
  },
  navItem:{
    flexDirection:'row',
    alignItems:'center',
    paddingVertical:12,
    borderBottomWidth:0.75,
    borderBottomColor:'#e8e8e4'
  },
  Txt:{
    fontSize:15,
    color:'#FFF',
    marginLeft:5,
  }
 
});