import React from 'react';
import {StyleSheet,View,Text,Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default function Header(){
  return (
    <>
      <View style={styles.Container}>
       <View style={styles.Top}>
           <Image source={require('../Imagem/Nubank_Logo.png')}/>
           <Text style={styles.Title}>Wellington</Text>
       </View>
       <Icon style={styles.icon} name="expand-more" size={24} color="#FFF"/>
       
      </View>
    </>
  );
};

const styles = StyleSheet.create({
    Container:{
    alignItems:'center',
    paddingVertical:30,
    },
  
    Top:{
    flexDirection:'row',
    alignItems:'center',
    marginBottom:50,
    },
    Title:{
    fontSize:18,
    color:'#FFF',
    fontWeight:'bold',
    marginLeft:8
    },
   icon:{
     marginTop:-50
   }
});
