import React from 'react';
import { SafeAreaView,StyleSheet,View} from 'react-native';
import Tabs from './componetes/Tabs'
import Header from './componetes/Header'
import Main from './componetes/Main'
import Menu from './componetes/Menu'
export default function App(){
 
  return (
    <>
      <SafeAreaView style={styles.corpo}>
        <View style={styles.container}>
          <Header/>
          <Menu/>
          <Main/>
          <Tabs/>
       </View>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  corpo:{
    flex:1,
    backgroundColor:'#8B10AE'
  },
  container:{
    flex:1,
    backgroundColor:'#8B10AE',
    justifyContent:'center'
    
  }
});


